import tensorflow as tf
from baselines.common import tf_util
from baselines.a2c.utils import fc
from baselines.common.distributions import make_pdtype
from baselines.common.input import observation_placeholder, encode_observation
from baselines.common.tf_util import adjust_shape
from baselines.common.mpi_running_mean_std import RunningMeanStd
from baselines.common.models import get_network_builder

import gym
import numpy as np
import matplotlib.pyplot as plt
import copy
from gym import spaces


class PolicyWithValue(object):

    def __init__(self, env=None, observations=None, pi_latent=None, pi_latent_attacked=None, encoded_x=None,
                 estimate_q=False, vf_latent=None, sess=None, adversaries=None, attack_args=None, build_latent=None,
                 **tensors):
        self.env = env
        self.X = observations
        self.encoded_x = encoded_x
        self.adversaries = adversaries
        self.attack_args = attack_args
        self.build_latent = build_latent
        self.state = tf.constant([])
        self.initial_state = None
        self.__dict__.update(tensors)

        vf_latent = vf_latent if vf_latent is not None else pi_latent

        vf_latent = tf.layers.flatten(vf_latent)
        pi_latent = tf.layers.flatten(pi_latent)
        pi_latent_attacked = tf.layers.flatten(pi_latent_attacked)

        # Based on the action space, will select what probability distribution type
        self.pdtype = make_pdtype(env.action_space)
        self.pdtype_attacked = make_pdtype(env.action_space)

        self.pd, self.pi = self.pdtype.pdfromlatent(pi_latent, init_scale=0.01)
        self.pd_attacked, self.pi_attacked = self.pdtype_attacked.pdfromlatent(pi_latent_attacked, init_scale=0.01)

        # Take an action
        self.action = self.pd.sample()
        self.action_attacked = self.pd_attacked.sample()

        # Calculate the neg log of our probability
        self.neglogp = self.pd.neglogp(self.action)
        self.neglogp_attacked = self.pd_attacked.neglogp(self.action_attacked)
        self.sess = sess or tf.get_default_session()

        if estimate_q:
            assert isinstance(env.action_space, gym.spaces.Discrete)
            self.q = fc(vf_latent, 'q', env.action_space.n)
            self.vf = self.q
        else:
            self.vf = fc(vf_latent, 'vf', 1)
            self.vf = self.vf[:, 0]

        # if not self.attack_args.use_attack and self.attack_args.other_attack_type:
        if self.attack_args.other_attack_type:
            if isinstance(env.action_space, spaces.Discrete):
                self.logdist = self.pd.logdist()
                self.other_adversaries = self._build_test_attack()
            # elif isinstance(env.action_space, spaces.Box):
            #     self.other_adversaries = self._build_test_attack_continuous()

    def _evaluate(self, variables, observation, **extra_feed):
        sess = self.sess
        feed_dict = {self.X: adjust_shape(self.X, observation)}
        for inpt_name, data in extra_feed.items():
            if inpt_name in self.__dict__.keys():
                inpt = self.__dict__[inpt_name]
                if isinstance(inpt, tf.Tensor) and inpt._op.type == 'Placeholder':
                    feed_dict[inpt] = adjust_shape(inpt, data)

        return sess.run(variables, feed_dict)

    def step(self, observation, **extra_feed):
        # self.state refers to the hidden state when using recurrent networks
        a, v, state, neglogp = self._evaluate([self.action, self.vf, self.state, self.neglogp], observation, **extra_feed)
        if state.size == 0:
            state = None
        return a, v, state, neglogp

    def step_attacked(self, observation, **extra_feed):
        # self.state refers to the hidden state when using recurrent networks
        a, v, state, neglogp = self._evaluate([self.action_attacked, self.vf, self.state, self.neglogp_attacked], observation, **extra_feed)
        if state.size == 0:
            state = None

        return a, v, state, neglogp

    def step_action_with_other_adversaries(self, observation, **extra_feed):
        # self.state refers to the hidden state when using recurrent networks
        adversaries = self._evaluate(self.other_adversaries, observation)
        a, state = self._evaluate([self.action, self.state], observation + adversaries, **extra_feed)
        if state.size == 0:
            state = None

        return a, state

    def get_adversaries(self, observation, **extra_feed):
        adversaries = self._evaluate(self.adversaries, observation, **extra_feed)
        return adversaries

    def get_other_adversaries(self, observation):
        adversaries = self._evaluate(self.other_adversaries, observation)
        return adversaries

    def value(self, ob, *args, **kwargs):
        return self._evaluate(self.vf, ob, *args, **kwargs)

    def save(self, save_path):
        tf_util.save_state(save_path, sess=self.sess)

    def load(self, load_path):
        tf_util.load_state(load_path, sess=self.sess)

    def _build_test_attack(self):
        attack_step_size = self.attack_args.attack_step_size
        attack_steps = self.attack_args.attack_steps
        epsilon = self.attack_args.epsilon

        encoded_x = tf.identity(self.encoded_x)
        noise = tf.random_normal(self.encoded_x.shape, 0, epsilon / 4)
        noise = tf.clip_by_value(noise, -epsilon / 2, epsilon / 2)
        encoded_x = tf.clip_by_value(encoded_x + noise, 0, 255)
        logdist_static = tf.stop_gradient(tf.log(tf.exp(self.logdist) + 1e-5))

        if self.attack_args.other_attack_type == 'random':
            adversaries = encoded_x - self.encoded_x
            print("using random noise")
        else:
            for h in range(attack_steps):
                policy_latent = self.build_latent(encoded_x)
                policy_dynamic, _ = self.pdtype.pdfromlatent(policy_latent, init_scale=0.01, reuse=tf.AUTO_REUSE)
                logdist_dynamic = policy_dynamic.logdist()

                if self.attack_args.other_attack_type == 'policy':
                    print("using policy attack")
                    loss = tf.reduce_sum(-tf.reduce_sum(tf.exp(logdist_dynamic) * logdist_static, 1))

                elif self.attack_args.other_attack_type == 'policy_max':
                    print("using policy_max attack")
                    loss = tf.reduce_sum(-tf.reduce_sum(
                        tf.one_hot(tf.argmax(logdist_static, 1), self.env.action_space.n) * logdist_dynamic, 1))

                elif self.attack_args.other_attack_type == 'policy_min':
                    print("using policy_min attack")
                    loss = -tf.reduce_sum(-tf.reduce_sum(
                        tf.one_hot(tf.argmin(logdist_static, 1), self.env.action_space.n) * logdist_dynamic, 1))
                else:
                    loss = None
                    print("unauthorized attack type when testing! exit!")
                    exit(0)

                noise = tf.sign(tf.gradients(loss, encoded_x)[0])
                encoded_x = encoded_x + attack_step_size * noise
                encoded_x = tf.minimum(tf.maximum(encoded_x, self.encoded_x - epsilon), self.encoded_x + epsilon)
                encoded_x = (tf.clip_by_value(encoded_x, 0, 255))

            adversaries = encoded_x - self.encoded_x

        return adversaries

    def _build_test_attack_continuous(self):
        attack_step_size = self.attack_args.attack_step_size
        attack_steps = self.attack_args.attack_steps
        epsilon = self.attack_args.epsilon

        encoded_x = tf.identity(self.encoded_x)
        noise = tf.random_normal(self.encoded_x.shape, 0, epsilon / 4)
        noise = tf.clip_by_value(noise, -epsilon / 2, epsilon / 2)
        encoded_x = tf.clip_by_value(encoded_x + noise, 0, 255)
        logdist_static = tf.stop_gradient(tf.log(tf.exp(self.logdist) + 1e-5))

        if self.attack_args.other_attack_type == 'random':
            adversaries = encoded_x - self.encoded_x
            print("using random noise")
        else:
            for h in range(attack_steps):
                policy_latent = self.build_latent(encoded_x)
                policy_dynamic, _ = self.pdtype.pdfromlatent(policy_latent, init_scale=0.01, reuse=tf.AUTO_REUSE)
                logdist_dynamic = policy_dynamic.logdist()

                if self.attack_args.other_attack_type == 'policy':
                    print("using policy attack")
                    loss = tf.reduce_sum(-tf.reduce_sum(tf.exp(logdist_dynamic) * logdist_static, 1))

                elif self.attack_args.other_attack_type == 'policy_max':
                    print("using policy_max attack")
                    loss = tf.reduce_sum(-tf.reduce_sum(
                        tf.one_hot(tf.argmax(logdist_static, 1), self.env.action_space.n) * logdist_dynamic, 1))

                elif self.attack_args.other_attack_type == 'policy_min':
                    print("using policy_min attack")
                    loss = -tf.reduce_sum(-tf.reduce_sum(
                        tf.one_hot(tf.argmin(logdist_static, 1), self.env.action_space.n) * logdist_dynamic, 1))
                else:
                    loss = None
                    print("unauthorized attack type when testing! exit!")
                    exit(0)

                noise = tf.sign(tf.gradients(loss, encoded_x)[0])
                # noise = tf.gradients(loss, encoded_x)[0] * 50000
                encoded_x = encoded_x + attack_step_size * noise
                encoded_x = tf.minimum(tf.maximum(encoded_x, self.encoded_x - epsilon), self.encoded_x + epsilon)
                encoded_x = (tf.clip_by_value(encoded_x, 0, 255))

            adversaries = encoded_x - self.encoded_x

        return adversaries


def build_policy(env, policy_network, value_network=None,  normalize_observations=False, estimate_q=False,
                 attack_args=None, **policy_kwargs):
    print("normalized observation", normalize_observations)
    if isinstance(policy_network, str):
        network_type = policy_network
        # print("network type", network_type, policy_kwargs)
        attack_kwargs = copy.deepcopy(policy_kwargs)
        attack_kwargs.update({'stack_attack_network': attack_args.stack_attack_network})
        policy_network = get_network_builder(network_type)(**policy_kwargs)
        if network_type == 'mlp':
            attack_network = get_network_builder('attack_net_mlp')(**attack_kwargs)
        else:
            if attack_args.stack_attack_network >= 1:
                attack_network = get_network_builder('attack_net')(**attack_kwargs)
            else:
                print("Unet is used to construct the attacking network", attack_args)
                attack_network = get_network_builder('Unet')(**attack_kwargs)
        value_network = get_network_builder(network_type)(**policy_kwargs)

    def build_latent(x):
        with tf.variable_scope('pi', reuse=tf.AUTO_REUSE):
            policy_latent = policy_network(x)
            return policy_latent

    def policy_fn(nbatch=None, nsteps=None, sess=None, observ_placeholder=None):
        ob_space = env.observation_space
        X = observ_placeholder if observ_placeholder is not None else observation_placeholder(ob_space, batch_size=nbatch)
        extra_tensors = {}

        if normalize_observations and X.dtype == tf.float32:
            encoded_x, rms = _normalize_clip_observation(X)
            extra_tensors['rms'] = rms
        else:
            encoded_x = X

        encoded_x = encode_observation(ob_space, encoded_x)

        # build attack hidden network (the input is encoded_x)
        with tf.variable_scope('at', reuse=tf.AUTO_REUSE):
            adversaries_tmp = attack_network(encoded_x) * attack_args.epsilon
            # print("check adversaries network", adversaries_tmp.shape, encoded_x.shape)
            # exit(0)
            adversaries = tf.clip_by_value(encoded_x + adversaries_tmp, 0, 255.0) - encoded_x

        # build policy hidden network (the input is encoded_x + use_attack * adversaries)
        with tf.variable_scope('pi', reuse=tf.AUTO_REUSE):
            policy_latent = policy_network(encoded_x)
            policy_latent_attacked = policy_network(encoded_x + adversaries)

        # building hidden value network (the input is encoded_x)
        assert callable(value_network)
        with tf.variable_scope('vf', reuse=tf.AUTO_REUSE):
            vf_latent = value_network(encoded_x)

        policy = PolicyWithValue(
            env=env,
            observations=X,
            encoded_x=encoded_x,
            pi_latent=policy_latent,
            pi_latent_attacked=policy_latent_attacked,
            vf_latent=vf_latent,
            adversaries=adversaries,
            attack_args=attack_args,
            build_latent=build_latent,
            sess=sess,
            estimate_q=estimate_q,
            **extra_tensors
        )
        return policy

    return policy_fn


def _normalize_clip_observation(x, clip_range=[-5.0, 5.0]):
    rms = RunningMeanStd(shape=x.shape[1:])
    norm_x = tf.clip_by_value((x - rms.mean) / rms.std, min(clip_range), max(clip_range))
    return norm_x, rms

