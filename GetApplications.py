# coding=utf-8
import os
import io
import subprocess
import json
import csv

from datetime import datetime

# cluster = 'rr2'
cluster = 'gcr'


def get_appID(state):
    appIDs = []
    response = os.popen(r'curl -k --ntlm --user : "{0}"'.format(
        'https://philly/api/list?clusterId=' + cluster + '&vcId=resrchvc&userName=v-wancha&numFinishedJobs=5000')).readlines()
    print(type(response), len(response))
    content = json.loads(response[0])

    print(content.keys(), len(content["finishedJobs"]), content['finishedJobsCount'], content['isChange_ignore_ElapsedTime_Progress_EvalErr'],
          content['lastChangeTime_ignore_ElapsedTime_Progress_EvalErr'], content['queuedJobs'])
    for i in range(len(content["runningJobs"])):
        if 'adversarial' in content['runningJobs'][i]['name']:
            print(content['runningJobs'][i]['name'], content['runningJobs'][i]['status'])

            name = content['runningJobs'][i]['name']
            name = name.split('!')[0]
            name = name.split('ns-p-')[1]
            appIDs.append([content['runningJobs'][i]['appID'], name])

    # response = os.popen(r'curl -k --ntlm --user : "{0}"'.format(
    #     'https://philly/api/list?clusterId=' + cluster + '&vcId=resrchvc&userName=v-wancha&numFinishedJobs=5000')).readlines()
    # print(type(response), len(response))
    # content = json.loads(response[0])
    # for i in range(len(content["finishedJobs"])):
    #     if 'ent=' in content['finishedJobs'][i]['name']:
    #         print(content['finishedJobs'][i]['name'], content['finishedJobs'][i]['status'])
    #
    #         name = content['finishedJobs'][i]['name']
    #         name = name.split('!')[0]
    #         name = name.split('ns-p-')[1]
    #         appIDs.append([content['finishedJobs'][i]['appID'], name])

    return appIDs


def save_appID(IDs, file):
    f = open(file, 'w+')
    for i in range(len(IDs)):
        f.write(IDs[i][0] + ' ')
        f.write(IDs[i][1] + '\n')
    f.close()
    exit(0)


def load_appID(file):
    f = open(file, 'r')
    IDs = []
    Names = []
    line = f.readline()
    while(line):
        tmp = line[:-1]
        IDs.append(tmp.split(' ')[0])
        Names.append(tmp.split(' ')[1])
        line = f.readline()
    f.close()
    return IDs, Names


if __name__ == '__main__':
    file = 'appIDs_max_min.txt'
    IDs = get_appID('appID')
    print("IDs", IDs)
    save_appID(IDs, file)

    path_user = r'//philly/' + cluster + '/resrchvc/v-wancha'
    path_apps = r'//philly/' + cluster + '/resrchvc/v-wancha/applications_max_min'
    path_data = r'/var/storage/shared/resrchvc/sys/jobs'

    Change_exist = True

    try:
        subprocess.call('mkdir ' + path_apps, shell=True)
    except:
        pass

    appIDs, appNames = load_appID(os.path.join(path_user, file))

    for app in os.listdir(path_data):
        found_apps = False
        for i in range(len(appIDs)):
            if appIDs[i] == app:
                found_apps = True
                name = appNames[i]
                name = name.split('noframeskip-v4')[0] + name.split('noframeskip-v4')[1]
                tmp = name.split('max-min-adversarial-')
                name = tmp[1]
                name = name + "_" + app
                break
        if found_apps:
            print("found expected apps", app)
            target_path = path_apps + '/' + name
            print("target path", target_path)
            # if the app already exists in the directory, remove it first or leave it unchanged
            if name in os.listdir(path_apps):
                print("file already exist")
                if Change_exist:
                    try:
                        subprocess.call('rm -rf ' + target_path, shell=True)
                        subprocess.call('mkdir ' + target_path, shell=True)
                        subprocess.call('cp -r ' + path_data + '/' + app + '/stdout' + ' ' + target_path, shell=True)
                        subprocess.call('cp ' + path_data + '/' + app + '/metadata.json' + ' ' + target_path, shell=True)
                    except:
                        pass
                else:
                    pass
            else:
                print("file does not exist before")
                try:
                    subprocess.call('mkdir ' + target_path, shell=True)
                    subprocess.call('cp -r ' + path_data + '/' + app + '/stdout' + ' ' + target_path, shell=True)
                    subprocess.call('cp ' + path_data + '/' + app + '/metadata.json' + ' ' + target_path, shell=True)
                except:
                    pass
            # exit(0)
