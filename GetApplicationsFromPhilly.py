# coding=utf-8
import os
import io
import subprocess
import json
import csv

from datetime import datetime

# cluster = 'rr2'
cluster = 'gcr'


def get_appID(state):
    appIDs = []
    response = os.popen(r'curl -k --ntlm --user : "{0}"'.format(
        'https://philly/api/list?clusterId=' + cluster + '&vcId=resrchvc&userName=v-wancha&numFinishedJobs=5000')).readlines()
    print(type(response), len(response))
    content = json.loads(response[0])

    # print(content.keys(), len(content["finishedJobs"]), content['finishedJobsCount'], content['isChange_ignore_ElapsedTime_Progress_EvalErr'],
    #       content['lastChangeTime_ignore_ElapsedTime_Progress_EvalErr'], content['queuedJobs'])
    for i in range(len(content["runningJobs"])):
        if 'both' in content['runningJobs'][i]['name']:
            print(content['runningJobs'][i]['name'], content['runningJobs'][i]['status'])

            name = content['runningJobs'][i]['name']
            name = name.split('!')[0]
            name = name.split('ns-p-')[1]
            appIDs.append([content['runningJobs'][i]['appID'], name])

    # response = os.popen(r'curl -k --ntlm --user : "{0}"'.format(
    #     'https://philly/api/list?clusterId=' + cluster + '&vcId=resrchvc&userName=v-wancha&numFinishedJobs=5000')).readlines()
    # print(type(response), len(response))
    # content = json.loads(response[0])
    # for i in range(len(content["finishedJobs"])):
    #     if 'both' in content['finishedJobs'][i]['name']:
    #         print(content['finishedJobs'][i]['name'], content['finishedJobs'][i]['status'])
    #
    #         name = content['finishedJobs'][i]['name']
    #         name = name.split('!')[0]
    #         name = name.split('ns-p-')[1]
    #         appIDs.append([content['finishedJobs'][i]['appID'], name])

    return appIDs


def save_appID(IDs, file):
    f = open(file, 'w+')
    for i in range(len(IDs)):
        f.write(IDs[i][0] + ' ')
        f.write(IDs[i][1] + '\n')
    f.close()
    exit(0)


def load_appID(file):
    f = open(file, 'r')
    IDs = []
    Names = []
    line = f.readline()
    while(line):
        tmp = line[:-1]
        IDs.append(tmp.split(' ')[0])
        Names.append(tmp.split(' ')[1])
        line = f.readline()
    f.close()
    return IDs, Names


if __name__ == '__main__':
    # file = 'appIDs_max_min.txt'
    # IDs = get_appID('appID')
    # print("IDs", IDs)
    # save_appID(IDs, file)

    path_apps = r'//philly/' + cluster + '/resrchvc/v-wancha/max_min_logs'
    target_path_base = r'//philly/' + cluster + '/resrchvc/v-wancha/applications_max_min_attack'

    try:
        subprocess.call('mkdir ' + target_path_base, shell=True)
    except:
        pass

    for app in os.listdir(path_apps):
        if "tra_typ=attack" in app:
            print("found expected apps", app)
            target_path = target_path_base + '/' + app
            print("target path", target_path)
            # if the app already exists in the directory, remove it first or leave it unchanged
            try:
                path_data = os.path.join(path_apps, app, 'log.txt')
                # print(os.listdir(os.path.join(path_apps, app)))
                # print("path_data", path_data)
                # subprocess.call('rm -rf ' + target_path, shell=True)
                subprocess.call('mkdir ' + target_path, shell=True)
                print("command line", 'cp -r ' + path_data + ' ' + target_path)
                subprocess.call('cp -r ' + path_data + ' ' + target_path, shell=True)
            except:
                pass

            # exit(0)
