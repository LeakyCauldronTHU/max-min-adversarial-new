import gym

if __name__=='__main__':

    env = gym.make("BoxingNoFrameskip-v4")
    env.reset()
    for i in range(100000):
        obs, rew, don, inf = env.step(env.action_space.sample())
        env.render()
        print(rew, don, inf)
        if don:
            break
